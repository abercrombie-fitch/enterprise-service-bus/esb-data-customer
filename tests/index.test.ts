// tests/index.test.ts

import app from '../src/index';
import request from 'supertest';
import chai from 'chai';

const expect = chai.expect;

describe('Test the root path', () => {
  before((done) => {
    const server = require('../src/server');
    done();
  });
  it('should respond to the GET method', () => {
    return request(app).get('/readiness').expect(200);
  });
  it('should respond with OK', () => {
    return request(app)
      .get('/readiness')
      .then((response) => {
        expect(response.text).to.equal('OK');
      });
  });
});
