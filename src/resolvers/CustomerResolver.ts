// src/resolvers/CustomerResolver.ts

import { Query, Resolver, Mutation, Arg } from 'type-graphql';
import { Customer } from '../models/Customer';
import { IResolver } from '../interfaces/IResolver';

// TODO: Error Handling, Logging, Metrics, Circuit Breaker
@Resolver()
export class CustomerResolver implements IResolver<Customer> {
  @Mutation(() => Customer)
  async create(@Arg('data') data: Customer): Promise<Customer> {
    const customer = Customer.create(data);
    await customer.save();
    return customer;
  }

  @Query(() => [Customer])
  read(): Promise<Customer[]> {
    return Customer.find();
  }

  @Mutation(() => Customer)
  async update(@Arg('id') id: number, @Arg('data') data: Customer): Promise<Customer> {
    const customer = await Customer.findOne({ where: { id } });

    if (!customer) throw new Error('Customer not found!');
    Object.assign(customer, data);
    await customer.save();
    return customer;
  }

  @Mutation(() => Boolean)
  async delete(@Arg('id') id: number): Promise<boolean> {
    const customer = await Customer.findOne({ where: { id } });

    if (!customer) throw new Error('Customer not found!');

    await customer.remove();
    return true;
  }
}
