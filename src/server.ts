// src/server.ts

import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { CustomerResolver } from './resolvers/CustomerResolver';
import { GraphQLSchema } from 'graphql';
import { createConnection } from 'typeorm';
import app from './index';

async function bootstrap() {
  // await createConnection();
  const schema: GraphQLSchema = await buildSchema({ resolvers: [CustomerResolver] });

  const apolloServer = new ApolloServer({ schema });

  console.log('Adding Apollo Server middleware');
  apolloServer.applyMiddleware({ app, path: '/graphql' });

  app.listen({ port: 3000 }, () => {
    console.log(`Server running on port: 3000`);
  });
}

bootstrap();
