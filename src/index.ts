// src/index.ts

import 'reflect-metadata';
import express from 'express';

import compression from 'compression';
import cors from 'cors';

const app = express();

app.use('*', cors());
app.use(compression());

app.get('/readiness', (request, response) => {
  return response.status(200).send('OK');
});

export default app;
