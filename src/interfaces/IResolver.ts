// src/interfaces/IResolver.ts

export interface IResolver<T> {
  create(data: T): Promise<T>;
  read(): Promise<T[]>;
  update(id: number, data: T): Promise<T>;
  delete(id: number): Promise<boolean>;
}
